/**
 * @file
 */

#include "smtp.h"
#include "utils.h"



bool SMTPConnection::auth(const std::string &username,
								  const std::string &password) {
	auto e = socket.createSSL(hostname, port);
	if (!e.succes) {
		std::cerr << e.error << std::endl;
		return false;
	}

	std::string out;
	socket.send("EHLO prateekkumar.in\r\n");


	if (!std::regex_search(socket.receive(), std::regex("^220")))return false;
	if (!std::regex_search(socket.receive(), std::regex("^250")))return false;
	if (!std::regex_search(socket.receive(), std::regex("^250")))return false;
	if (!std::regex_search(socket.receive(), std::regex("^250")))return false;
	if (!std::regex_search(socket.receive(), std::regex("^250")))return false;
	if (!std::regex_search(socket.receive(), std::regex("^250")))return false;
	if (!std::regex_search(socket.receive(), std::regex("^250")))return false;
	if (!std::regex_search(socket.receive(), std::regex("^250")))return false;
	if (!std::regex_search(socket.receive(), std::regex("^250")))return false;

	if(!socket.send("AUTH LOGIN\r\n")){
		return false;
	}
	out = socket.receive();
	if (!std::regex_search(out, std::regex("^334 "))) {
		return false;
	}
	socket.send(utils::Encode(username) + "\r\n");
	out = socket.receive();
	if (!std::regex_search(out, std::regex("^334 "))) {
		return false;
	}
	socket.send(utils::Encode(password) + "\r\n");
	out = socket.receive();
	if (!std::regex_search(out, std::regex("^235 "))) {
		std::cerr << "SMTPConnection::auth failed" << std::endl;
		return false;
	}
	return true;
}

bool SMTPConnection::send(const std::string &from, const std::string &to,
								  const std::string &subject, const std::string &body) {
	std::string out;

	socket.send("MAIL FROM: " + from + "\r\n");
	out = socket.receive();
	if (!std::regex_search(out, std::regex("^250 ")))  return false;

	socket.send("RCPT TO: " + to + "\r\n");
	out = socket.receive();
	if (!std::regex_search(out, std::regex("^250 "))) return false;

	socket.send("DATA\r\n");
	out = socket.receive();
	if (!std::regex_search(out, std::regex("^354 "))) return false;

	std::string body2 = body;
	if (body.size() < 5 || body.substr(body.size() - 5, 5) != "\r\n.\r\n") {
		body2 += "\r\n.\r\n";
	}

	socket.send(body2);
	out = socket.receive();
	if (!std::regex_search(out, std::regex("^250 "))) return false;

	return true;
}
