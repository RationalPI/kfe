#include "httpserver.h"

#include <QCoreApplication>
#include <QNetworkInterface>
#include <iostream>
#include <QTcpSocket>
#include <sstream>


HttpServer::HttpServer(QObject *parent) : QObject(parent){
	server = new QTcpServer(this);
	// waiting for the web brower to make contact,this will emit signal
	connect(server, &QTcpServer::newConnection,[this](){
		auto socket = server->nextPendingConnection();

		int timeout=7*1000;
		while(!(socket->waitForReadyRead(50))){
			if(timeout-=50 < 0){
				socket->deleteLater();
				return ;
			}
		}

		auto request=QString(socket->readLine(1000));

		if(request.contains("GET")){
			auto urlParts=request.split(" ")[1].split("?");
			QString route,args;
			if(!urlParts.empty()){
				route=urlParts[0];
			}
			if(urlParts.size()>1){
				args=urlParts[1];
			}


			if(routeHandlers.count(route)){
				routeHandlers[route](socket,args);
			}else {
				//logging unknown routes
				std::cout<<"HttpServer::GET "<<route.toStdString()<< " args " << args.toStdString() << " from " << socket->peerAddress().toString().toStdString() << std::endl;

				std::stringstream ss;
				ss <<"HTTP/1.1 200 OK\r\n"
				<<"Content-Type: text/html\r\n"
				<<"Connection: close\r\n\r\n"
				<<"<!DOCTYPE html>"
				<<"<html>"
//				<<"  <head>"
				  //				<<"	 <meta http-equiv=\"refresh\" content=\"1; url='https://www.w3docs.com'\" />"
				  //				<<"  </head>"
				<<"  <body>"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"	 <img src=\"https://c.tenor.com/VFFJ8Ei3C2IAAAAM/rickroll-rick.gif\">"
				<<"  </body>"
				<<"</html>";
				ss.flush();
				socket->write(ss.str().c_str());
			}
		}


		/*info for dbg*/{
			/*
			char webBrowerRXData[1000];
			int sv=socket->read(webBrowerRXData,1000);
			std::cout<<"\nreading web browser data="<<std::endl;
			for(int i=0;i<sv;i++){
				std::cout<<webBrowerRXData[i];
			}
			std::cout<<std::endl;
			*/
		}

		socket->flush();
		socket->deleteLater();
	});

	if(!server->listen(QHostAddress::Any,80)){
		std::cout << "Web server could not start: "<< server->errorString().toStdString() <<std::endl;
	}else{
		std::cout << "Web server is waiting for a connection on port 80"<<std::endl;
	}
}
