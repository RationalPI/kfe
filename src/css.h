#pragma once

const auto css= R"(
body {/*background and font for all the page*/
        background-image: linear-gradient(90deg, #3a807e, #4c2368);
        font-family: 'lato', sans-serif;
        color: #e8e6e3
}

.container {/*the main layout*/
        max-width: 1000px;
        margin-left: auto;
        margin-right: auto;
        padding-left: 10px;
        padding-right: 10px
}

form ul li button a {
    font-size: 17px;
    color: #e8e6e3;
    text-decoration: none;
}

form ul li button {
    background-color: rgba(24, 26, 27, .3);
    padding: 0px 5px;
}

button {
    background-color: rgba(24, 26, 27, .3);
    border: none;
    color: #e8e6e3;
    padding: 6px 13px;
    text-align: center;
    text-decoration: none;
    display: nline-block;
    font-size: 17px;
    border-radius: 5px;
    margin-bottom: 5px;
    margin-right: 5px;
}
button:hover {
    background-color: #007759 !important;
    transition: all 0.4s ease 0s;
}


/*horyzontal form liste*/
form ul { list-style-type: none; }
form ul li { display: inline-block }



h2 {/*titles*/
        font-size: 26px;
        margin: 20px 0;
        text-align: center
}

.responsive-table li {/*every lines in the table*/
        border-radius: 3px;
        padding: 5px;
        display: flex;
        justify-content: space-between;
        margin-bottom: 5px
}

.responsive-table .table-row {/*a table line*/
        background-color: rgba(24, 26, 27, .3);
        box-shadow: rgba(0, 0, 0, .1) 0px 0px 9px 0px
}
.responsive-table .table-row:hover {
    background-color: rgba(24, 26, 27, .5);
}

.responsive-table .table-header {/*the table title*/
        background-color: #444d72;
        font-size: 16px;
        font-weight: bold;
        text-transform: uppercase;
}

.responsive-table a{ /*a link in the table*/
    color: #e8e6e3;
}
.responsive-table a:hover{ /*a link in the table*/
    color: #747371;
    transition: all 0.4s ease 0s;
}

.responsive-table .col-1 { flex-basis: 20% }
.responsive-table .col-2 { flex-basis: 20% }
.responsive-table .col-3 { flex-basis: 60% }

@media all and (max-width:767px) {
        .responsive-table .table-header {
                display: none
        }
        .responsive-table li {
                display: block
        }
        .responsive-table .col {
                flex-basis: 100%
        }
        .responsive-table .col {
                display: flex;
                padding: 10px 0
        }
        .responsive-table .col:before {
                color: #6c7a89;
                padding-right: 10px;
                content: attr(data-label);
                flex-basis: 50%;
                text-align: right
        }
}

)";
