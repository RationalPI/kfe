#pragma once
#include <QTcpServer>
#include <functional>

struct HttpServer : public QObject{
	explicit HttpServer(QObject *parent = 0);
	inline void setRoute(const QString& route, std::function<void(QTcpSocket*,QString)> f){routeHandlers[route]=f;}
	inline void clearRoutes(){routeHandlers.clear();}
private:
	QTcpServer *server;
	std::map<QString,std::function<void(QTcpSocket*,QString/*args*/)>> routeHandlers;
};
