// MIT License
// Copyright (c) 2022 Nicolas SALMIERI
#pragma once

/*usage:
	A local member does the same things as a member for a class but is more scoped.

	struct Foo:protected LocalMemberOwner{
		void funcWithPreDestructor(){
			static LocalMember<std::vector<int*>> localMember;
			localMember.setPreDestructor([](std::vector<int*>& val){...});

			localMember(this) = ...;
			localMember(this).clear();
			...
		}
		void func(){
			static LocalMember<Bar> localMember;

			localMember(this);
		}
		void funcWithStandardValue(){
			static LocalMember<Bar> localMember;

			localMember(this,Bar());
		}
	};




*/

#include <map>
#include <vector>
#include <functional>

struct localMemberOwner;

//! LocalMemberOwner in charge of releasing LocalMembers
struct localMemberOwner{
	//! interface for releasing memory
	class I_LocalMember{
	protected:
		void addOwner(localMemberOwner* owner);
		virtual void releaseLocalMembers(localMemberOwner* owner)=0;
		friend localMemberOwner;
	};

	virtual ~localMemberOwner();
private:
	friend I_LocalMember;
	std::vector<I_LocalMember*> ownedMembers;
};


//! A local member does the same things as a member for a class but is more scoped.
template<class T>
struct LocalMember:private localMemberOwner::I_LocalMember{
	T& operator ()(localMemberOwner* owner){
		if(!values.count(owner)){
			addOwner(owner);
		}
		return values[owner];
	}
	T& operator ()(localMemberOwner* owner,T&& defaultValue){
		if(!values.count(owner)){
			values.emplace(std::make_pair(owner,std::move(defaultValue)));
			addOwner(owner);
		}
		return values[owner];
	}

	void setPreDestructor(std::function<void(T&)> dest){
		customDestructor=dest;
	}


private:
	std::function<void(T&)> customDestructor=[](T&){};
	std::map<localMemberOwner*,T> values;

	void releaseLocalMembers(localMemberOwner* owner) override{
		if(values.count(owner)/*in case of double deletion*/){
			customDestructor(values[owner]);
			values.erase(owner);
		}
	}
};

#define LM_TAGED(val)val ## _LM
#define localMember(type,name) static LocalMember<type> LM_TAGED(name); auto& name=LM_TAGED(name)(this);


bool testLocalMember();
