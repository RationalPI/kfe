#include "LocalMember.h"

void localMemberOwner::I_LocalMember::addOwner(localMemberOwner* owner){
	owner->ownedMembers.push_back(this);
}

localMemberOwner::~localMemberOwner(){
	for (auto& ownedMember : ownedMembers) {
		ownedMember->releaseLocalMembers(this);
	}
}

bool testLocalMember(){
	enum class states{
		TestClassConstructor=0,
		TestClassMoveConstructor=1,
		TestClassDestructor=2,
		PreDestructor=3,
	};

	static std::vector<states> log;


	struct TestClass{
		TestClass(TestClass&& ){log.push_back(states::TestClassMoveConstructor);}
		TestClass(){log.push_back(states::TestClassConstructor);}
		~TestClass(){log.push_back(states::TestClassDestructor);}
	};


	struct Foo:protected localMemberOwner{
		void funcWithPreDestructor(){
			static LocalMember<TestClass> aze;

			aze(this);
			aze.setPreDestructor([](TestClass& val){log.push_back(states::PreDestructor);});
		}
		void standardUsage(){
			localMember(TestClass,aze);
		}
		void funcWithoutPreDestructorWithStandardValue(){
			static LocalMember<TestClass> aze;

			aze(this,TestClass());
		}
	};

	bool ok=true;

	// standard
	log.clear();
	/*RAII*/{
		Foo f;
		f.standardUsage();
		/*check that it is constructed only once and then can be used*/{
			f.standardUsage();
			f.standardUsage();
			f.standardUsage();
		}

		ok=ok&&log==std::vector<states>{states::TestClassConstructor};
	}
	ok=ok&&log==std::vector<states>{states::TestClassConstructor,states::TestClassDestructor};

	// standard with default value
	log.clear();
	/*RAII*/{
		Foo f;
		f.funcWithoutPreDestructorWithStandardValue();
		ok=ok&&log==std::vector<states>{
				states::TestClassConstructor,
				states::TestClassMoveConstructor,
				states::TestClassMoveConstructor,
				states::TestClassDestructor,
				states::TestClassDestructor};
	}
	ok=ok&&log==std::vector<states>{
			states::TestClassConstructor,
			states::TestClassMoveConstructor,
			states::TestClassMoveConstructor,
			states::TestClassDestructor,
			states::TestClassDestructor,
			//the final destruction
			states::TestClassDestructor};

	// standard with default value and usage
	log.clear();
	/*RAII*/{
		Foo f;
		f.funcWithoutPreDestructorWithStandardValue();
		ok=ok&&log==std::vector<states>{
				states::TestClassConstructor,
				states::TestClassMoveConstructor,
				states::TestClassMoveConstructor,
				states::TestClassDestructor,
				states::TestClassDestructor};

		f.funcWithoutPreDestructorWithStandardValue();
		ok=ok&&log==std::vector<states>{
				states::TestClassConstructor,
				states::TestClassMoveConstructor,
				states::TestClassMoveConstructor,
				states::TestClassDestructor,
				states::TestClassDestructor,
				//standard value is constructed and then trown as it is not used
				states::TestClassConstructor,
				states::TestClassDestructor,};
	}
	ok=ok&&log==std::vector<states>{
			states::TestClassConstructor,
			states::TestClassMoveConstructor,
			states::TestClassMoveConstructor,
			states::TestClassDestructor,
			states::TestClassDestructor,
			//standard value is constructed and then trown as it is not used
			states::TestClassConstructor,
			states::TestClassDestructor,
			//the final destruction
			states::TestClassDestructor};


	//pre destructor
	log.clear();
	/*RAII*/{
		Foo f;
		f.funcWithPreDestructor();
		ok=ok&&log==std::vector<states>{states::TestClassConstructor};
	}
	ok=ok&&log==std::vector<states>{states::TestClassConstructor,states::PreDestructor,states::TestClassDestructor};

	return ok;
}
