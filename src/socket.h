#pragma once

#include <QSslSocket>
#include <regex>
#include <QThread>
#include <queue>
#include <mutex>

/**
 * @brief Socket class
 * @details Socket class that creates TCP connection to
 * a server and saves the state, has methods to send and
 * receive messages from server.
 *
 */
struct Socket{
    ~Socket();

	struct SocketCreateState{bool succes; std::string error;};
    /**
        * @brief Create function creates a socket
        * @details Create function creates a TCP Connection
        * over which the messages and sent to and received from.
        *
        * @param hostname The hostname of the server to connect to.
        * @param port The port of the server to connect to.
        *
        * @return Status of creation and any error message if failed to
        * create socket.
        */
	 SocketCreateState createSSL(std::string hostname, int port);

    /**
        * @brief Send function sends a message to the server.
        * @details The message is sent to the server over a TCP connection.
        *
        * @param s The message that is to be sent.
        * @return Status whether the send was successful.
        */
    bool send(std::string s);

    /**
        * @brief Receive function receives a message from the server.
        * @details A thread receives in parallel and stores the message
        * in a queue from which this function returns one.
        * @return The message received from the server.
        */
    std::string receive();

    /**
        * @brief Receive function receives a message from the server
        * @details This function pops all those messages form the queues until
        * some message matches the regex provided.
        *
        * @param rgx The regex which should be matched with message.
        * @return The message received form the server.
        */
    std::string receive(std::regex rgx);

	 bool alive(){
		 // better safe than sorry
		 return socket->state()==QAbstractSocket::ConnectedState;
	 }

private:
	 std::unique_ptr<QSslSocket> socket;
	 std::unique_ptr<QThread> receiver;
    std::queue<std::string> messages;
    std::mutex messagesMtx;
};
