#include "application.h"

#include "css.h"
#include "imap.h"
#include "smtp.h"
#include <QCoreApplication>
#include <QProcess>
#include <random>

#include <QFileInfo>
#include <QUrl>

Application::Application():httpServer(this){
	auto persistanceFI=QFileInfo(config.persistenceFirlePath);
	if(persistanceFI.exists()&&persistanceFI.size()!=0){
		std::ifstream is(config.persistenceFirlePath.toStdString());
		cereal::JSONInputArchive archive( is );
		archive(cereal::make_nvp("KfeApplication::Model", this->model));
	}

	/**/{
		auto flushNotifies=[this]{
			if(!pendingNotifyMessages.empty()){
				SMTPConnection smtp(config.smtp_server, config.smtp_port);
				if(!smtp.auth(config.appEmail, config.appEmailPassword)){
					Log << "Application::flushNotifies, SMTPConnection::auth failed!" << std::endl;
					return;
				}

				auto ToLat1=[](const std::string& s){
					return QString::fromStdString(s).toLatin1().toStdString();
				};
				std::string subject="Kfé";
				std::string from_ = "<" + config.appEmail + ">";

				Log << "Application::flushNotifies["<< std::endl;

				while (!pendingNotifyMessages.empty()) {
					auto& messageToSend=pendingNotifyMessages.front();

					std::string body =
							"From: " + config.appEmailName + " <" + config.appEmail + ">\r\n"+
							"To: " + messageToSend.mailAddress + "\r\n"+
							"Subject: " + subject + "\r\n"+
							"MIME-Version: 1.0\r\n"+
							"Content-Type: text/plain\r\n"+
							"\r\n" +
							messageToSend.message + "\r\n\r\n.\r\n";

					static const auto space="    ";

					if(!smtp.send(from_, messageToSend.mailAddress, ToLat1(subject), ToLat1(body))){
						Log << space<<"smtp.send error";
						break;
					}else{

						Log << space<<"sent to: " << messageToSend.mailAddress  << std::endl
							 << space<<"message: " << messageToSend.message << std::endl << std::endl;

						pendingNotifyMessages.pop_front();
					}
				}

				Log << "]" << std::endl;
			}
		};
		constexpr auto thirtySec = 30*1000;

		connect(&thirtySecTimer,&QTimer::timeout,[=](){
			thirtySecTimer.start(thirtySec);
			flushNotifies();
		});
		thirtySecTimer.setSingleShot(false);
		thirtySecTimer.start(0);
	}

	/**/{

		connect(&fiveMinutesTimer,&QTimer::timeout,[this](){
			constexpr auto fiveMinuts = 5*60*1000;
			fiveMinutesTimer.start(fiveMinuts);
			every5minuts();
		});
		fiveMinutesTimer.setSingleShot(false);
		fiveMinutesTimer.start(0);
	}

	/**/{
		constexpr auto msecsPerDay = 24*60*60*1000;


		connect(&at_notificationTime_Timer,&QTimer::timeout,[=](){
			at_notificationTime_Timer.start(msecsPerDay);
			at_notificationTime();
		});
		at_notificationTime_Timer.setSingleShot(false);

		int msecsUntil8AM = QTime::currentTime().msecsTo(QTime(CTS::notifyTime[0],CTS::notifyTime[1],CTS::notifyTime[2]));
		if (msecsUntil8AM < 0) msecsUntil8AM += msecsPerDay;
		at_notificationTime_Timer.start(msecsUntil8AM);
	}

	updateSchedule();
}

void Application::saveModel(){
	Log << "Application::saveModel()" << std::endl;
	std::ofstream os(config.persistenceFirlePath.toStdString());
	cereal::JSONOutputArchive archive( os );
	archive(cereal::make_nvp("KfeApplication::Model", this->model));
}

void httpFrameHeader(std::stringstream& ss,int refresh=-1){
	ss<<"HTTP/1.1 200 OK\r\n";
	ss<<"Content-Type: text/html\r\n";
	ss<<"Connection: close\r\n";
	if(refresh>0){ss<<"Refresh: "<<refresh<<"\r\n";}
	ss<<"\r\n<!DOCTYPE html>\r\n";
}

void Application::updateHttpRoutes(){
	httpServer.clearRoutes();

	httpServer.setRoute("/favicon.ico",[](QTcpSocket* socket,QString){
		std::stringstream ss;
		httpFrameHeader(ss,10);
		ss
				<<"<html lang='en'><head><meta charset='UTF-8'><meta name='apple-mobile-web-app-title' content='CodePen'></head>"
				<<"<body translate='no'>"
				<<"404"
				<<"</body></html>";

		ss.flush();
		socket->write(QString::fromStdString(ss.str()).replace("\n","</br>").toStdString().c_str());

		ss.flush();
		socket->write(ss.str().c_str());
	});

	httpServer.setRoute("/planning",[this](QTcpSocket* socket,QString){
		std::stringstream ss;
		httpFrameHeader(ss,30);
		ss<<"<html lang='en'><head><meta charset='UTF-8'><meta name='apple-mobile-web-app-title' content='CodePen'><style>"<< css <<"</style></head>"
        <<"		<body translate='no'>"
        <<"		  <div class='container'>"
        <<"		  <h2>Planning Kfé</h2>"
        <<"		  <ul class='responsive-table'>"
        <<"			 <li class='table-header'>"
        <<"				<div class='col col-1'>Jour</div>"
        <<"				<div class='col col-2'>Date</div>"
        <<"				<div class='col col-3'>Responsable</div>"
        <<"			 </li>";
		for (auto& worckDay : model.schedule) {
			auto workerLabelByID=[this](const Model::WorkerID& wId){
				std::string label="⏳";
				for (auto& w : model.workerPool) {
					if(w.id==wId){
						label=w.userName;
					}
				}
				return label;
			};
			static const QLocale francais(QLocale::French, QLocale::France);
			auto specialFormat=worckDay.day.dayOfWeek()==dayOfWeek::Mon?std::make_pair("<b><font color=#7faeff>","</font></b>"):std::make_pair("","");
			std::string dayIcone;
			switch (worckDay.day.dayOfWeek()) {
			case dayOfWeek::Mon:dayIcone="🏀 ";break;
			case dayOfWeek::Tue:dayIcone="🎲 ";break;
			case dayOfWeek::Wed:dayIcone="🇬🇷 ";break;
			case dayOfWeek::Thu:dayIcone="🎲 ";break;
			case dayOfWeek::Fri:dayIcone="🍺 ";break;
			default:dayIcone="";
			}


			ss <<"<li class='table-row'>"
                <<"  <div class='col col-1' >"<<dayIcone<<specialFormat.first<<francais.toString(worckDay.day,"dddd").toStdString()<<specialFormat.second<<"</div>"
				<<"  <div class='col col-2' >"<<specialFormat.first<<francais.toString(worckDay.day,"dd MMMM").toStdString()<<specialFormat.second<<"</div>"
				<<"  <div class='col col-3' >"<<specialFormat.first<<workerLabelByID(worckDay.worker)<<specialFormat.second<<"</div>"
				<<"</li>";
		}
		ss <<"</ul>"
			<<"</br><a href='https://www.rrc.ca/its/help-resources/change-windows-10-default-mail-app-to-outlook/'><button>Configuration Email</button></a>"
			<<"</br><a href='mailto:"<<config.appEmail<<"?subject=Inscription&body=%2B1:VotreNomIci'><button>Inscription</button></a>"
			<<"ou envoyez \"+1:Votre Nom\" @ "<<config.appEmail << " (Vous pourrez vous renommer dans votre page perso)"
			<<"</br><a href='mailto:"<<config.appEmail<<"?subject=Désinscription&body=-1'><button>Désinscription</button></a>"
			<<"ou envoyez \"-1\" @ "<<config.appEmail
		  <<"		</div>"
			<<"</body>"
			<<"</html>";

		ss.flush();
		socket->write(ss.str().c_str());
	});
	httpServer.setRoute(QString("/administration/")+config.password,[this](QTcpSocket* socket,QString){
		std::stringstream ss;
		httpFrameHeader(ss,10);
		ss <<"<html lang='en'><head><meta charset='UTF-8'><meta name='apple-mobile-web-app-title' content='CodePen'></head>"
			<<"<body translate='no'>"
			<<"<pre><code>";
		/*raii*/{
			cereal::JSONOutputArchive archive( ss );
			archive(cereal::make_nvp("KfeApplication::Model", this->model));
		}
		ss <<"</pre></code></body></html>";

		ss.flush();
		socket->write(QString::fromStdString(ss.str()).replace("\n","</br>").toStdString().c_str());

		ss.flush();
		socket->write(ss.str().c_str());
	});

	httpServer.setRoute(QString("/log/")+config.password,[this](QTcpSocket* socket,QString){
		std::stringstream ss;
		httpFrameHeader(ss,10);
		ss <<"<html lang='en'><head><meta charset='UTF-8'><meta name='apple-mobile-web-app-title' content='CodePen'></head>"
			<<"<body translate='no'>"
			<<"<pre><code>";

		//		QByteArray ret;
		//		/*raii*/{
		//			auto tail=std::make_shared<QProcess>(this);
		//			tail->setProgram("tail");
		//			tail->setArguments({"-n999"});
		//			tail->waitForStarted();

		//			auto journal=std::make_shared<QProcess>(this);
		//			journal->setProgram("journalctl");
		//			journal->setArguments({"-u","kfe.service"});
		//			journal->setStandardOutputProcess(tail.get());
		//			journal->waitForStarted();


		//			tail->setReadChannel(QProcess::ProcessChannel::StandardOutput);
		//			tail->waitForFinished();

		//			tail->open();

		//			ret=tail->readAll();
		//		}
		//		ss << ret.toStdString();


		ss <<"</pre></code></body></html>";

		ss.flush();
		socket->write(QString::fromStdString(ss.str()).replace("\n","</br>").toStdString().c_str());

		ss.flush();
		socket->write(ss.str().c_str());
	});


	for (auto& worker : model.workerPool) /*routes de page perso*/{
		static const std::string deleteStr="delete";
		static const std::string dispoStr="dispo";
		static const std::string forgetStr="forget";
		static const std::string renameStr="rename";
		const auto usrRoute="/usr/"+worker.id;

		auto worckDayAnnulationArgs=[](const Model::WorkDay& wDay){
			return "?"+deleteStr+"&"+wDay.day.toString(CerelaQDateFormat).toStdString();
		};

		httpServer.setRoute(QString::fromStdString(usrRoute),[this,worker,usrRoute,worckDayAnnulationArgs](QTcpSocket* socket,QString args){
			bool mutated=false;

			auto& workerRef=model.workerById(worker.id);
			/*args handling*/{
				if(args.contains(QString::fromStdString(deleteStr))){
					for (auto& wDay : model.schedule) /*routes d'annulation*/{
						auto wouldBeArgs=QString::fromStdString(worckDayAnnulationArgs(wDay));
						if(wouldBeArgs.contains(args)&&wDay.worker==workerRef.id){
							notifyWorkerAssignement(
										wDay.day,
										wDay.worker,
										AssignemntModality::Cancellation
										);

							model.workDayByDate(wDay.day).blackListedWorkers.push_back(workerRef.id);
							model.workDayByDate(wDay.day).worker=CTS::Nobody;

							mutated=true;
							break;
						}
					}
				}else if(args.contains(QString::fromStdString(dispoStr))){
					std::vector<dayOfWeek> newDisponibilities;
					for (auto& arg : args.split("&")) {
						auto argParts=arg.split("=");
						if(argParts.size()==2 && argParts[1]=="on"){
							auto day=dayOfWeek(argParts[0].toInt());
							if(QVector<dayOfWeek>(CTS::dayOfWeek).count(day)){
								newDisponibilities.push_back(day);
							}
						}
					}

					if(newDisponibilities!=workerRef.disponibilities){
						workerRef.disponibilities=newDisponibilities;
						mutated=true;
					}
				}else if(args.contains(QString::fromStdString(renameStr))){
					auto argsToString=[&args](){
						const auto dummyUrlStarter="http://dummy?";
						const auto toRemoveFromUrl=QString::fromStdString(renameStr+"=");
						auto querry=QUrl(dummyUrlStarter+args);
						return querry.query(QUrl::FullyDecoded).replace("+"," ").replace(toRemoveFromUrl,"").toStdString();
					};
					workerRef.userName=argsToString();
					mutated=true;
				}else if(args.contains(QString::fromStdString(forgetStr))){
					for (auto& wDay : model.schedule){
						auto& blackliste=model.workDayByDate(wDay.day).blackListedWorkers;
						blackliste.erase(std::remove(blackliste.begin(),blackliste.end(),workerRef.id),blackliste.end());
						mutated=true;
					}
				}
			}

			std::stringstream ss;
			httpFrameHeader(ss);

			ss
					<<"<html lang='en'><head><meta charset='UTF-8'><meta name='apple-mobile-web-app-title' content='CodePen'><style>"
				<<css
			  <<"		</style></head>"
            <<"		"
            <<"		<body translate='no'>"
            <<"		  <div class='container'>"
				<<"		  <h2>"<<workerRef.userName<<"</h2>";
			if(!mutated){
				/*planning*/{
					ss <<"		  <ul class='responsive-table'>"
				<<"			 <li class='table-header'>"
				<<"				<div class='col col-1'>Jour</div>"
				<<"				<div class='col col-2'>Date</div>"
				<<"				<div class='col col-3'></div>"
				<<"			 </li>"
				<<R"(
                    <script>
                       function DisplayDesistementsConfirm() {
                          var newLine = "\r\n"
                          var msg ="";
                          msg += "Voulez-vous vous désister ?";
                          msg += newLine;
                          msg += "Evitez les desistements au denier moment...";
                          msg += newLine;
                          msg += "Vous allez être disponible pour une autre date !";
                          if (confirm(msg)){return true;}else{event.stopPropagation(); event.preventDefault();};
                       }
                    </script>
                    )";

					for (auto& worckDay : model.schedule) {
						if(worckDay.worker==workerRef.id){

							static const QLocale francais(QLocale::French, QLocale::France);
							ss   <<"<li class='table-row'>"
						  <<"   <div class='col col-1' >"<<francais.toString(worckDay.day,"dddd").toStdString()<<"</div>"
						  <<"   <div class='col col-2' >"<<francais.toString(worckDay.day,"dd MMMM").toStdString()<<"</div>"
						  <<"   <div class='col col-3' >"
						  <<"   <a href='"<<usrRoute<<worckDayAnnulationArgs(worckDay)<<"' onclick='DisplayDesistementsConfirm()'>Désistement</a>"
						  <<"</div>"
						  <<"</li>";
						}
					}
				}

				/*dispo*/{
					static const std::map<dayOfWeek,std::string> dayToString={
						{dayOfWeek::Mon,"Lundi"},
						{dayOfWeek::Tue,"Mardi"},
						{dayOfWeek::Wed,"Mercredi"},
						{dayOfWeek::Thu,"Jeudi"},
						{dayOfWeek::Fri,"Vendredi"},
					};

					ss <<"</ul>"
						<<"<form method='GET' action='"<<usrRoute<<"'><ul>"
						<<"<li><button><a href='"<<usrRoute<<"?"<<forgetStr<<"'>Oublier les désistements</a></button></li>"
						<<"<li><button type='submit'>Envoyer les disponibilités</button></li>"
						// this invisiable radio Types dispo requests (if all checkbox deselected there is that)
					  <<"<li><input style='opacity:0;' type='radio' id='"<<dispoStr<<"' name='"<<dispoStr<<"' checked/></li>";

					for (auto& day : CTS::dayOfWeek) {
						bool dispo=QVector<dayOfWeek>::fromStdVector(workerRef.disponibilities).count(day);
						ss<<"<li><input type='checkbox' id='"<<day<<"' name='"<<day<<"' "<<(dispo?"checked":"")<<"/><label for='"<<day<<"'></input>"<<dayToString.at(day)<<"</label></li>";
					}
					ss <<"</ul></form>";
				}
				/*rename*/{
					ss <<"</ul>"
                       <<"<form method='GET' action='"<<usrRoute<<"'><ul>"
                       <<"<li><button type='submit'>Renommer</button></li>"
                       <<"<style>textarea {resize: none;}</style>"
                       <<"<input type='text' id='"<<renameStr<<"' name='"<<renameStr<<"' required minlength='5' maxlength='60' size='60' placeholder='"<<workerRef.userName<<"'>"
                       <<"</ul></form>";
				}

				ss <<"</br><a href='/planning'><button>Planning</button></a>";
			}else{
				ss << "<meta http-equiv='refresh' content='0; URL="<<usrRoute<<"'>";
				updateSchedule();
			}

			//fin
			ss <<"</div>"
            <<"</body>"
            <<"</html>";

			ss.flush();
			socket->write(ss.str().c_str());
		});
	}
}

void Application::updateSchedule(){
	auto today=QDate::currentDate();

	/*cleaning old days*/
	if(!model.schedule.empty()){
		while (true) {
			if(model.schedule.front().day < today){
				model.schedule.pop_front();
				if(model.schedule.empty()) break;
			}else{
				break;
			}
		}
	}

	/*provisioning workdays*/{
		while (model.schedule.size() < CTS::provisioningDayCount) {
			QDate nxtDayCandidate;
			if(model.schedule.empty()){
				nxtDayCandidate=today.addDays(1);
			}else{
				nxtDayCandidate=model.schedule.back().day.addDays(1);
			}

			while (std::count(CTS::banedDayOfWeek.begin(),CTS::banedDayOfWeek.end(),nxtDayCandidate.dayOfWeek())) {
				nxtDayCandidate=nxtDayCandidate.addDays(1);
			}

			model.schedule.push_back(Model::WorkDay());
			model.schedule.back().day=nxtDayCandidate;
		}
	}

	auto assigneWorkers=[this](Model::WorkDay& wDay){
		// we just don't work at all if there is no more than a weak planable
		// nobody should work twice a weak
		if(model.workerPool.size()<(dayOfWeek::Sun-CTS::banedDayOfWeek.size())){
			return;
		}
		if(wDay.worker!=CTS::Nobody){
			return;
		}


		std::vector<Model::Worker> randomOrderAvalabelWorkers;{
			auto workersIdsInSchedule=model.workerIdsInSchedule();
			for (auto& worker : model.workerPool) {
				if(!std::count(workersIdsInSchedule.begin(),workersIdsInSchedule.end(),worker.id)){
					randomOrderAvalabelWorkers.push_back(worker);
				}
			}

			static thread_local std::mt19937 rng{std::random_device{}()};
			std::shuffle(randomOrderAvalabelWorkers.begin(),randomOrderAvalabelWorkers.end(),rng);
		}

		for (auto& AvalabelWorker : randomOrderAvalabelWorkers) {
			auto weakDayCompatible=QVector<dayOfWeek>::fromStdVector(AvalabelWorker.disponibilities).count(dayOfWeek(wDay.day.dayOfWeek()));
			auto notBalckListed=!QVector<Model::WorkerID>::fromStdVector(wDay.blackListedWorkers).count(AvalabelWorker.id);
			if(weakDayCompatible && notBalckListed){
				wDay.worker=AvalabelWorker.id;

				notifyWorkerAssignement(
							wDay.day,
							wDay.worker,
							AssignemntModality::JustSelected
							);
				break;
			}
		}
	};

	for (auto& wDay : model.schedule) {
		assigneWorkers(wDay);
	}

	updateHttpRoutes();
	saveModel();
}

void Application::every5minuts(){
#ifdef NO_EMAIL
	return;
#endif

	IMAPConnection imap(config.imap_server, config.imap_port);
	if (!imap.login(config.appEmail, config.appEmailPassword)){
		Log << "IMAPConnection::auth failed!" << std::endl;
		return;
	}

	for (auto& mailID : imap.getAllmails("inbox")) {
		auto mail=imap.getMail("inbox",mailID);
		Log << "Application::every5minuts::mail.from: " << mail.from << std::endl;
		//			Log
		//					<< "############################################"<< std::endl
		//					<< "every5minuts::mail.from: " << mail.from << std::endl
		//					<< "every5minuts::mail.to: " << mail.to << std::endl
		//					<< "every5minuts::mail.subject: " << mail.subject << std::endl
		//					<< "every5minuts::mail.text: " << mail.text << std::endl
		//					<< "############################################"<< std::endl
		//					<< std::endl;

		auto senderMailAddress="<"+QString::fromStdString(mail.from).split("<").back().toStdString();



		/*handling message body*/{
			bool valid=false;
			for (auto& mailLine : QString::fromStdString(mail.text).split("\n")) { if(valid)break;
				if(mailLine.contains("+1:")){
					Model::WorkerID newID;{
						static thread_local std::mt19937 g{std::random_device{}()};
						static thread_local std::uniform_int_distribution<size_t> d{0,size_t(0)-1};

						auto existingIds=model.workerIdsInSchedule();
						do {
							newID=QString::number(d(g)).toStdString();
						} while (std::count(existingIds.begin(),existingIds.end(),newID));
					}
					Model::Worker newWorker(
								newID,
								mailLine.split(":")[1].split("\n").front().toStdString(),
							senderMailAddress
							);

					addWorker(newWorker);
					valid=true;
				}else if(mailLine.contains("-1")){
					for (auto& existingW : model.workerPool) {
						if(existingW.mail==senderMailAddress){
							removeWorker(existingW.id);
							valid=true;
							break;
						}
					}
				}
			}

			if(!valid){
				if(!QString::fromStdString(mail.from).contains(config.appEmail.c_str())){
					notify(senderMailAddress,"help:\n+1:UserName\n-1\n\nJe ne comprends pas: "+mail.text);
				}
			}

			imap.deleteMail(mail);
		}
	}
}

void Application::addWorker(Model::Worker w){
	for (auto& existingW : model.workerPool) {
		if(existingW.mail==w.mail){
			removeWorker(existingW.id);
		}
	}
	model.workerPool.push_back(w);
	notifyWorker(w.id,"Vous faites partie de la liste des producteurs de Kfé :)");
	updateSchedule();
}

void Application::removeWorker(const Model::WorkerID& wId){
	for (auto& wDay : model.schedule) {
		if(wDay.worker==wId){
			notifyWorkerAssignement(
						wDay.day,
						wDay.worker,
						AssignemntModality::Cancellation
						);
			wDay.worker=CTS::Nobody;
		}
	}

	notifyWorker(wId,"Vous ne faites plus partie de la liste des producteurs de Kfé :'(");

	auto& v=model.workerPool;
	v.erase(std::remove_if(v.begin(),v.end(),[&wId](const Model::Worker& w){
		return w.id==wId;
	}),v.end());

	updateSchedule();
}

void Application::at_notificationTime(){
	Log << "Application::at_notificationTime" << std::endl;
#ifdef NO_EMAIL
	return;
#endif
	auto today=QDate::currentDate();
	for (auto& workDay : model.schedule) {
		if(today.daysTo(workDay.day)==1){
			notifyWorkerAssignement(
						workDay.day,
						workDay.worker,
						AssignemntModality::Tomorrow
						);
		}
		if(today.daysTo(workDay.day)==0){
			notifyWorkerAssignement(
						workDay.day,
						workDay.worker,
						AssignemntModality::Today
						);
		}
	}

	updateSchedule();
}

void Application::notifyWorkerAssignement(const QDate& day, const Model::WorkerID& worker, Application::AssignemntModality mod){
	static const QLocale francais(QLocale::French, QLocale::France);

	auto dayStr="Le "+francais.toString(day,"dddd dd MMMM");
	switch (mod) {
	case AssignemntModality::Cancellation:
		notifyWorker(worker,(dayStr+", Annulation de la date.").toStdString());
		break;
	case AssignemntModality::JustSelected:
		notifyWorker(worker,(dayStr+", Vous serez de Kfé.").toStdString());
		break;
	case AssignemntModality::Today:
		notifyWorker(worker,(dayStr+", C'est maintenant, au boulot :)").toStdString());
		break;
	case AssignemntModality::Tomorrow:
		notifyWorker(worker,(dayStr+", C'est demain, vous serez de Kfé, n'oubliez pas. :)").toStdString());
		break;
	}
}

void Application::notify(const std::string& mailAddress, std::string msg){
	Log << "Application::notify[" << std::endl << mailAddress << std::endl << msg << std::endl<< "]"<< std::endl;
	pendingNotifyMessages.push_back(PendingMessage{mailAddress,msg});
}

void Application::notifyWorker(const Model::WorkerID& worker, std::string msg){
	const auto& w = model.workerById(worker);
	std::string notMsg="Bonjour, "+msg+" \n Votre page perso est http://"+config.appIp+"/usr/"+worker;
	notify(w.mail,notMsg);
}
