#include "socket.h"
#include <iostream>

Socket::SocketCreateState Socket::createSSL(std::string hostname, int port) {
	if(!QSslSocket::supportsSsl()){
		qDebug() << "QtSslSocket: supportsSsl" << QSslSocket::supportsSsl();
		qDebug() << "QtSslSocket: sslLibraryBuildVersionString" << QSslSocket::sslLibraryBuildVersionString();
		qDebug() << "QtSslSocket: sslLibraryVersionString" << QSslSocket::sslLibraryVersionString();
		throw std::string("no ssl support");
	}

	{
		bool initialized=false;
		std::mutex initializedMtx;

		receiver=std::unique_ptr<QThread>(QThread::create([this,&initialized,&initializedMtx,hostname,port]() {
			auto thisThred=QThread::currentThread();
			socket=std::unique_ptr<QSslSocket>(new QSslSocket);
			socket->connectToHostEncrypted(QString::fromStdString(hostname),port);
			socket->waitForConnected();
			socket->waitForEncrypted();

			initializedMtx.lock();
			initialized=true;
			initializedMtx.unlock();

			char buf[1024];
			std::string reply = "";
			while (true && !thisThred->isInterruptionRequested()) {
				socket->waitForReadyRead(200);
				int bytesRead = socket->read(buf, sizeof(buf) - 1);
				if (bytesRead < 0) {
					std::cout << "ssl read failed" << std::endl;
					return;
				}
				if(bytesRead>0){
					buf[bytesRead] = '\0';
					reply += std::string(buf);
					messagesMtx.lock();
					do {
						std::size_t found = reply.find("\r\n");
						if (found != std::string::npos) {
							messages.push(reply.substr(0, found));
							reply = reply.substr(found + 2);
						}else{
							break;
						}
					} while (true);
					messagesMtx.unlock();
				}
			}
			socket->close();
		}));

		receiver->start();

		while (true) {
			initializedMtx.lock();
			if(initialized){
				initializedMtx.unlock();
				break;;
			}
			initializedMtx.unlock();
		}
	}

	if(socket->state()!=QAbstractSocket::ConnectedState){
		std::cerr << "[ERROR] Socket::createSSL: " << socket->errorString().toStdString() << std::endl;
		return SocketCreateState{false, socket->errorString().toStdString()};
	}
	return SocketCreateState{true,""};
}

bool Socket::send(std::string s) {
	return socket->write(s.c_str(), s.size())!=-1;
}

Socket::~Socket() {
	receiver->requestInterruption();
	while (!receiver->isFinished()) {
		QThread::msleep(10);
	}
}

std::string Socket::receive() {
	std::string msg;
	while (true) {
		messagesMtx.lock();
		if (!messages.empty()) {
			msg = messages.front();
			messages.pop();
			messagesMtx.unlock(); break;
		}
		messagesMtx.unlock();
	}
	return msg + "\r\n";
}

std::string Socket::receive(std::regex rgx) {
	std::string msg = "";
	while (true) {
		std::string line = receive();
		msg += line;
		line = line.substr(0, line.size() - 2);
		if (std::regex_match(line, rgx)) {
			break;
		}
	}
	return msg;
}
