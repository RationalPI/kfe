#pragma once

#include "httpserver.h"
#include <QTimer>
#include <QDate>
#include <array>

#include <fstream>
#include <cereal/archives/json.hpp>
#include <cereal/types/list.hpp>
#include <cereal/types/vector.hpp>

enum dayOfWeek{
	Mon = 1,
	Tue = 2,
	Wed = 3,
	Thu = 4,
	Fri = 5,
	Sat = 6,
	Sun = 7,
};

namespace CTS {
	static constexpr auto provisioningDayCount=20;
	static constexpr auto banedDayOfWeek={dayOfWeek::Sat,dayOfWeek::Sun};
	static constexpr auto dayOfWeek={dayOfWeek::Mon,dayOfWeek::Tue,dayOfWeek::Wed,dayOfWeek::Thu,dayOfWeek::Fri};
	static constexpr auto Nobody="notAWorker";
	static constexpr std::array<int,3> notifyTime={6,0,0};//!< h,m,s
	};

constexpr auto CerelaQDateFormat="dd.MM.yyyy";

template<class A> void save(A& ar, const QDate& m, const std::uint32_t version){
	ar( cereal::make_nvp("Date", m.toString(CerelaQDateFormat).toStdString()));
}
template<class A> void load(A& ar, QDate & m, const std::uint32_t version){
	std::string tmp;
	ar( cereal::make_nvp("Date",tmp) );
	m=QDate::fromString(QString::fromStdString(tmp),CerelaQDateFormat);
}

#include "LocalMember.h"


struct Application:public QObject,private localMemberOwner{
	Application();
private:
	struct Model{
		using  WorkerID=std::string;
		struct Worker{
			Worker(){}
			Worker(WorkerID id,std::string userName,std::string mail):id(id),userName(userName),mail(mail){}
			WorkerID id;
			std::string userName;//!< just a label ...
			std::string mail;
			std::vector<dayOfWeek> disponibilities=CTS::dayOfWeek;
			std::vector<QDate> worked;
			template<class A> void serialize( A & ar ,const std::uint32_t version){
				ar( CEREAL_NVP(mail), CEREAL_NVP(userName), CEREAL_NVP(id), CEREAL_NVP(disponibilities), CEREAL_NVP(worked));
			}
		};
		struct WorkDay{
			QDate day;
			WorkerID worker=CTS::Nobody;
			std::vector<WorkerID> blackListedWorkers;
			template<class A> void serialize( A & ar ,const std::uint32_t version){
				ar( CEREAL_NVP(day), CEREAL_NVP(worker), CEREAL_NVP(blackListedWorkers));
			}
		};

		std::vector<Worker> workerPool={Worker("nsa","Nicolas SALMIERI","1salmieri.nicolas@gmail.com")};
		std::list<WorkDay> schedule;//!< (front is earlier) liste of WorkDay ordered from day n to day n+1 in time

		std::vector<WorkerID> workerIdsInSchedule()const{
			std::vector<WorkerID> ret;
			for (auto& wd : schedule) {
				if(wd.worker!=CTS::Nobody){
					ret.push_back(wd.worker);
				}
			}
			return ret;
		}

		const Worker& workerById(const WorkerID& id) const {
			return std::find_if(workerPool.begin(),workerPool.end(),[&id](const Worker& w){
				return w.id==id;
			}).operator*();
		}
		Worker& workerById(const WorkerID& id) {
			return std::find_if(workerPool.begin(),workerPool.end(),[&id](const Worker& w){
				return w.id==id;
			}).operator*();
		}

		WorkDay& workDayByDate(const QDate& date){
			return std::find_if(schedule.begin(),schedule.end(),[&date](const WorkDay& w){
				return w.day==date;
			}).operator*();
		}

		template<class A> void serialize( A & ar ,const std::uint32_t version){
			ar(CEREAL_NVP(workerPool), CEREAL_NVP(schedule) );
		}
	};

	void saveModel();
	void updateHttpRoutes();

	void updateSchedule();
	void every5minuts();
	void addWorker(Model::Worker w);
	void removeWorker(const Model::WorkerID& wId);

	void at_notificationTime();

	//! QDate::dayOfWeek => 1 = Monday to 7 = Sunday
	enum class AssignemntModality{
		JustSelected,
		Tomorrow,
		Today,
		Cancellation,
	};
	void notifyWorkerAssignement(const QDate& day, const Model::WorkerID& worker,AssignemntModality mod);
	void notifyWorker(const Model::WorkerID& worker,std::string msg);
	void notify(const std::string& mailAddress,std::string msg);

	Model model;
	QTimer at_notificationTime_Timer,fiveMinutesTimer,thirtySecTimer;

	struct Config {
		const QString persistenceFirlePath="kfe.json";
		const std::string imap_server="imap.gmail.com";//!<Hostname of IMAP server
		const int imap_port=993;//!<Port of IMAP server
		const std::string smtp_server="smtp.gmail.com";//!<Hostname of SMTP server
		const int smtp_port=465;//!<Port of SMTP server
		const std::string appIp="10.17.20.98";
		const std::string appEmail="kfe.rennes@gmail.com";//!<Username of the user
		const std::string appEmailPassword="psfszvdxybluofdf";//!<Password of the user
		const std::string appEmailName="kfé CEN";//!<Name of the user
		const QString password="0000";//!<Name of the user
	}config;
	HttpServer httpServer;

	std::ostream& Log=std::cout;

	struct PendingMessage{std::string mailAddress,message;};
	std::list<PendingMessage> pendingNotifyMessages;
};
