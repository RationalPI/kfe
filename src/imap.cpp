/**
 * @file
 */

#include "imap.h"
#include <sstream>

IMAPConnection::IMAPConnection(const std::string &hostname, int port):hostname(hostname),port(port) {
	id_string = "kfeRennesApp";
	rgx = std::regex("kfeRennesApp ((OK)|(NO)|(BAD)) .*");
}

bool IMAPConnection::login(const std::string &username,
									const std::string &password) {
	auto e = socket.createSSL(hostname, port);
	if (!e.succes) {
		std::cerr << e.error << std::endl;
		return false;
	}

	std::string command =
			id_string + " login " + username + " " + password + "\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);

	if(check_response(response, id_string)){
		return true;
	}
	std::cerr << "IMAPConnection::login failed: " << response << std::endl;
	return false;
}

/**
 * Returns 0 if BAD or NO is received
 * Returns 1 if OK received
 */
bool IMAPConnection::check_response(const std::string &response,
												const std::string &id_string) {

	std::string OKrgx = id_string + " [Oo][Kk]";
	std::string NOrgx = id_string + " [Nn][Oo]";
	std::string BADrgx = id_string + " [Bb][Aa][Dd]";

	if (regex_search(response, std::regex(OKrgx))) return 1;

	if (regex_search(response, std::regex(NOrgx))) return 0;

	if (regex_search(response, std::regex(BADrgx))) {
		return 0;
	}

	std::cerr << "DONT: " << response << std::endl;

	exit(1);

	return 1;
}

bool IMAPConnection::createMailbox(const std::string &mailbox) {
	std::string command = id_string + " create " + mailbox + "\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	return check_response(response, id_string);
}

bool IMAPConnection::deleteMailbox(const std::string &mailbox) {
	std::string command = id_string + " delete " + mailbox + "\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	return check_response(response, id_string);
}

bool IMAPConnection::renameMailbox(const std::string &oldmailbox,
											  const std::string &newmailbox) {
	std::string command =
			id_string + " rename " + oldmailbox + " " + newmailbox + "\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	return check_response(response, id_string);
}

bool IMAPConnection::noop() {
	std::string command = id_string + " noop\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	return check_response(response, id_string);
}

std::tuple<int, int, int> IMAPConnection::getCount(const std::string &mailbox) {
	std::string command = id_string + " examine " + mailbox + "\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	bool response_id = check_response(response, id_string);
	if (response_id) {
		std::smatch sm;
		int a = 0, b = 0, c = 0;
		if (regex_search(response, sm, std::regex("([0-9]+) EXISTS"))) {
			a = std::stoi(sm[1]);
		}
		if (regex_search(response, sm, std::regex("([0-9]+) RECENT"))) {
			b = std::stoi(sm[1]);
		}
		if (regex_search(response, sm, std::regex("UNSEEN ([0-9]+)"))) {
			c = std::stoi(sm[1]);
		}
		return std::make_tuple(a, b, c);
	} else
		return std::make_tuple(-1, -1, -1);
}

bool IMAPConnection::deleteMail(Mail mail) {
	bool response_id = select(mail.mailbox);
	if (!response_id) return response_id;

	std::string command = id_string + " uid store " + std::to_string(mail.uid) +
			" +FLAGS (\\Deleted)\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	if (!response_id) return response_id;

	command = id_string + " uid expunge " + std::to_string(mail.uid) + "\r\n";
	socket.send(command);
	response = socket.receive(rgx);
	return check_response(response, id_string);
}

std::vector<int> IMAPConnection::search(
		const std::string &mailbox, const std::string &from, const std::string &to,
		const std::string &subject, const std::string &text,
		const std::string &nottext, const std::string &since,
		const std::string &before) {
	std::vector<int> uids;

	bool response_id = select(mailbox);
	if (!response_id) {
		return uids;
	}

	std::string command = id_string + " uid search";
	if (from != "") command += " from \"" + from + "\"";
	if (to != "") command += " to \"" + to + "\"";
	if (subject != "") command += " subject \"" + subject + "\"";
	if (text != "") command += " text \"" + text + "\"";
	if (since != "") command += " since \"" + since + "\"";
	if (before != "") command += " before \"" + before + "\"";
	if (nottext != "") command += " not text \"" + nottext + "\"";

	command += "\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	if (!response_id) {
		return uids;
	}

	std::regex number("([0-9]+)");
	std::smatch sm;

	while (regex_search(response, sm, number)) {
		uids.push_back(std::stoi(sm[1]));
		response = sm.suffix();
	}

	return uids;
}

std::vector<Mail> IMAPConnection::getUnseenMails(const std::string &mailbox) {
	std::vector<Mail> mails;

	bool response_id = select(mailbox);
	if (!response_id) {
		return mails;
	}

	std::string command = id_string + " uid search unseen\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	if (response_id) {
		std::regex number("([0-9]+)");
		std::smatch sm;
		std::vector<int> uids;

		while (regex_search(response, sm, number)) {
			uids.push_back(std::stoi(sm[1]));
			response = sm.suffix();
		}

		std::vector<int>::iterator it = uids.begin();
		for (; it != uids.end(); it++) {
			mails.push_back(getMail(mailbox, *it));
		}

		// Fetching mails from servers makes them seen
		// To restore the original position, we remove the flag Seen from these
		// mails
		it = uids.begin();
		for (; it != uids.end(); it++) {
			command = id_string + " uid store " + std::to_string(*it) +
					" -flags (\\Seen) \r\n";
			socket.send(command);
			std::string response = socket.receive(rgx);
			response_id = check_response(response, id_string);
		}
	}
	return mails;
}

std::vector<Mail> IMAPConnection::getTopMails(const std::string &mailbox,
                                                             size_t k) {
	std::vector<Mail> mails;

	bool response_id = select(mailbox);
	if (!response_id) {
		return mails;
	}

	std::string command = id_string + " uid search all\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	if (response_id) {
		std::regex number("([0-9]+)");
		std::smatch sm;
		std::vector<int> uids;

		while (regex_search(response, sm, number)) {
			uids.push_back(std::stoi(sm[1]));
			response = sm.suffix();
		}

		if (uids.size() < k) k = uids.size();
		std::vector<int>::iterator it = uids.end() - k;
		for (; it != uids.end(); it++) {
			mails.push_back(getMail(mailbox, *it));
		}
	}
	return mails;
}

bool IMAPConnection::moveMail(const std::string &old_mailbox, int uid,
										const std::string &new_mailbox) {
	bool response_id = select(old_mailbox);
	if (!response_id) return false;

	std::string command = id_string + " uid copy " + std::to_string(uid) + " " +
			new_mailbox + "\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	if (!response_id) return false;

	command = id_string + " uid store " + std::to_string(uid) +
			" +flags (\\Deleted)\r\n";
	socket.send(command);
	response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	if (!response_id) return false;

	command = id_string + " uid expunge " + std::to_string(uid) + "\r\n";
	socket.send(command);
	response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	return response_id;
}

std::vector<std::string> IMAPConnection::getmailboxes() {
	std::vector<std::string> mailboxes;

	std::string command = id_string + " list \"\" *\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	bool response_id = check_response(response, id_string);
	if (response_id) {
		std::regex name("\"/\" \"(.*)\"\r\n");
		std::smatch sm;

		while (regex_search(response, sm, name)) {
			mailboxes.push_back(sm[1]);
			response = sm.suffix();
		}
	}

	return mailboxes;
}

bool IMAPConnection::select(const std::string &mailbox) {
	std::string command = id_string + " select " + mailbox + "\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	return check_response(response, id_string);
}

std::vector<int> IMAPConnection::getAllmails(std::string mailbox) {
	std::vector<int> uids;

	bool response_id = select(mailbox);
	if (response_id) {
		std::string command = id_string + " uid search all\r\n";
		socket.send(command);
		std::string response = socket.receive(rgx);
		response_id = check_response(response, id_string);
		if (response_id) {
			std::regex number("([0-9]+)");
			std::smatch sm;
			response.erase(std::remove(response.begin(), response.end(), '\r'),
								response.end());
			std::replace(response.begin(), response.end(), '\n', ' ');
			std::stringstream res(response);

			while (!res.eof()) {
				std::string id_str;
				res >> id_str;
				if (std::regex_match(id_str, number)) {
					uids.push_back(std::stoi(id_str));
				}
			}
		}
	}

	return uids;
}

Mail IMAPConnection::getMail(const std::string &mailbox, const int uid) {
	Mail mail;
	mail.mailbox = mailbox;

	bool response_id = select(mailbox);
	if (!response_id) {
		mail.uid = -1;
		return mail;
	}

	std::string command = id_string + " uid fetch " + std::to_string(uid) +
			" (BODY[HEADER.FIELDS (from to subject date)])\r\n";
	socket.send(command);
	std::string response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	if (response_id) {
		std::smatch sm;
		if (regex_search(response, sm, std::regex("From: (.*)\r\n")))
			mail.from = sm[1];
		if (regex_search(response, sm, std::regex("Date: (.*)\r\n")))
			mail.date = sm[1];
		if (regex_search(response, sm, std::regex("Subject: (.*)\r\n")))
			mail.subject = sm[1];
		if (regex_search(response, sm, std::regex("To: (.*)\r\n"))) mail.to = sm[1];
		/*regex_match(response, sm, std::regex("[.\\r\\n]*From: (.*)\\nDate: (.*)"
		  "\\nSubject: (.*)\\nTo: (.*)[.\\r\\n]*"));

	 mail.date = sm[2];
	 mail.subject = sm[3];
	 mail.to = sm[4];*/
	} else {
		mail.uid = -1;
		return mail;
	}

	mail.uid = uid;

	command =
			id_string + " uid fetch " + std::to_string(mail.uid) + " body[1]\r\n";
	socket.send(command);
	response = socket.receive(rgx);
	response_id = check_response(response, id_string);
	if (response_id) {
		response.erase(std::remove(response.begin(), response.end(), '\r'),
							response.end());
		std::stringstream ss(response);
        std::vector<std::string> responcePartes;



        std::string s = "";


        std::getline(ss, s);
		while (std::getline(ss, s)) {
            responcePartes.push_back(s);
		}

        responcePartes.pop_back();
        responcePartes.pop_back();
		mail.text = "";
        for (auto &responceParte : responcePartes) {
            mail.text += responceParte + "\n";
		}
	} else {
		mail.text = "\n";
	}

	return mail;
}

std::vector<Mail> IMAPConnection::getMails(const std::string &mailbox,
														 std::vector<int> uids) {
	std::vector<Mail> mails;

	for (auto it = uids.begin(); it != uids.end(); it++) {
		mails.push_back(getMail(mailbox, *it));
	}

	return mails;
}
