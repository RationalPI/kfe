cmake_minimum_required (VERSION 3.1)
project (Kfe)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_definitions(-DNO_EMAIL)

include_directories(src)
include_directories(3rd/cereal/include)
file(GLOB SOURCES "src/*.cpp")
file(GLOB HEADERS "src/*.h")
add_executable(Kfe ${SOURCES} ${HEADERS})

find_package(QT NAMES Qt6 Qt5 COMPONENTS Core REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Core REQUIRED)
find_package(Qt${QT_VERSION_MAJOR} COMPONENTS Network REQUIRED)
target_link_libraries(Kfe PRIVATE
    Qt${QT_VERSION_MAJOR}::Core
    Qt${QT_VERSION_MAJOR}::Network
    )

if(UNIX)
    target_link_libraries(Kfe PRIVATE -lpthread)
endif()

add_custom_target(OtherFiles SOURCES
    .gitignore
    LICENSE
    README.md
)

