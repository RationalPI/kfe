# C++ Client for IMAP and SMTP

## How to install

```sh
git clone https://gitlab.com/RationalPI/kfe.git
cd kfe
mkdir build
cd build
cmake ..
```

## How to run

```sh
cd build
./client
```

